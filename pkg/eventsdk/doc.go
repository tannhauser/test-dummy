//go:binary-only-package

/*
Package eventsdk provides utils for developers to generate and to propagate Marvin Events.
The Marvin Events are compatible to CloudEvents V02, and the eventsdk leverages CloudEvents SDK (https://github.com/cloudevents/sdk-go) to generate and encode Marvin Events.
The eventsdk propagate Marvin Events to Marvin platform using mutual TLS to meet the requirement of mutual authentication and data confidentiality.

Besides, the eventsdk also provide some information extracting functions for debug purpose.

The following sample gives an example of how to use this SDK to send out Marvin Events.

	package main

	import "encoding/json"

	import "github.com/pnetwork/marvin.eventsdk.go/pkg/eventsdk"

	func main() {
		marshal, err := json.Marshal(struct{
			Event string `json:"event"`
			Date string `json:"date:`
		}{
			Event: "test",
			Date: "2019-01-01T00:00:00Z",
		})
		if err != nil {
			// some error handling
		}
		opt := &eventsdk.Options {
			Type: "com.example.test.type",
			Source: "come.example.test.source",
			Data: marshal,
			DataContentType: "application/json",
		}
		e, err := eventsdk.NewEvent(opt)
		if err != nil {
			// some error handling
		}

		response, err := e.Send()
		if err != nil {
			// some error handling
		}
		// some other logics
	}
*/
package eventsdk
